import {
    StyleSheet, Platform
} from 'react-native';
import { scale, verticalScale, moderateScale } from '../utils/scale';
import Colors from '../../Common/Colors';





export const IssuesStyles = StyleSheet.create({

    text:{
        fontFamily:'Avenir-Medium',fontSize:scale(25),color:'white'
      },

    BurttonStyle: {
        marginBottom:5,
        backgroundColor:Colors.color.themeYellow,
        borderRadius:10,
        flexDirection:'row',
        flex:1,
        justifyContent:"space-around",
        alignItems:"center"
      },
    view: {
        margin:scale(15),justifyContent:"space-evenly",flex:0.7,width:"80%"
    },
    container: {
        flex: 1,
    backgroundColor: "white",
    justifyContent: "space-evenly",
    alignItems: "center"
    },


})