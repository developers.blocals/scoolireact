import {
    StyleSheet, Platform
} from 'react-native';
import { scale, verticalScale, moderateScale } from '../utils/scale';





export const LoginStyles = StyleSheet.create({

    logoStyles: {
        height: scale(155),
        width: scale(125),
        alignSelf: 'center',
       marginVertical: verticalScale(50),
        padding: 0
    },

    textInputStyle: {
        width: '89%',
        color: '#fff',
        marginLeft: scale(21),
        marginEnd: scale(20),
        justifyContent: 'center',

        paddingTop: 0,
        borderColor: '#fff',
        borderBottomWidth: scale(1)
    },
    SubmitButtonStyle: {
        alignSelf:"center",
        justifyContent:"center",
        backgroundColor:"#FF3662",
         alignItems: 'center',
        height: moderateScale(54),
         width:'70%',
         borderRadius:30,
         marginVertical:5
    },
    SignInTextStyle: {
        textAlign: 'center',
        fontFamily: 'Avenir-Medium',
        fontSize: scale(16),
        color: 'white'
    },


})