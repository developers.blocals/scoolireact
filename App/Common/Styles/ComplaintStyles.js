import {
    StyleSheet, Platform
} from 'react-native';
import { scale, verticalScale, moderateScale } from '../utils/scale';
import Colors from '../../Common/Colors';





export const ComplaintStyles = StyleSheet.create({

    text:{
        color:Colors.color.themeRed,
        fontWeight:"600",
        fontSize:scale(15),marginTop:0
      },

    textInputStyle: {
        width: '100%',
        height:verticalScale(140),
        borderRadius:15,
        fontSize: scale(15),
        backgroundColor:Colors.color.card,
        padding:scale(10),
        fontFamily: 'Avenir-Book',
        marginVertical: verticalScale(10),
      },
    SubmitButtonStyle: {
        alignSelf:"center",
        justifyContent:"center",
        backgroundColor:"#FF3662",
        alignItems: 'center',
       height: moderateScale(54),
         width:'92%',
         borderRadius:10,
         marginVertical:5
    },
    SignInTextStyle: {
        textAlign: 'center',
        fontFamily: 'Avenir-Medium',
        fontWeight:"bold",fontSize: scale(19),
        color: 'white'
    },


})