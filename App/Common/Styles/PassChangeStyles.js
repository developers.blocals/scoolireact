import {
    StyleSheet, Platform
} from 'react-native';
import { scale, verticalScale, moderateScale } from '../utils/scale';





export const PassChangeStyles = StyleSheet.create({

    logoStyles: {
        height: scale(105),
        width: scale(85),
        alignSelf: 'flex-end',
        marginEnd: scale(20),
        marginBottom: verticalScale(30),
        marginTop: verticalScale(30),
        padding: 0
    },
    successStyles: {
        height: scale(105),
        width: scale(85),
        alignSelf: 'center',

        marginBottom: verticalScale(30),
        marginTop: verticalScale(30),
        padding: 0
    },


    textInputStyle: {
        width: '89%',
        color: '#fff',
        marginLeft: scale(21),
        marginEnd: scale(20),
        justifyContent: 'center',

        paddingTop: 0,
        borderColor: '#fff',
        borderBottomWidth: scale(1)
    },
    SubmitButtonStyle: {


        alignItems: 'center',
        height: moderateScale(65),
        marginEnd: scale(20),
        flexDirection: 'row',
        marginLeft: scale(35),
        width: '90%',


        borderBottomColor: 'transparent',


    },
    SignInTextStyle: {
        textAlign: 'center',
        width: '90%',
        marginBottom: verticalScale(10),
        alignSelf: 'center',
        fontFamily: 'Avenir-Medium',
        marginEnd: scale(10),
        fontSize: scale(16),
        color: 'white'
    },


})