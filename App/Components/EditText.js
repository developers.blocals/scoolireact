import {
    TouchableOpacity,
    View,
    Image, Text,TextInput,
    StyleSheet,ImageBackground
  } from 'react-native';
  
  import React from 'react';
import { moderateScale, verticalScale, scale } from '../Common/utils/scale';
import { ComplaintStyles } from '../Common/Styles/ComplaintStyles';

 

  export var EditText = (placeholder,url,onChange,value) => {
    return (
        <ImageBackground resizeMode='contain'source={require('../MobileApp/Images/LoginImages/edittxt_bg.png')} 
         style={{
            textAlign:'left',
            alignItems: 'center',
            height:moderateScale(68),
            marginEnd:scale(20),
            flexDirection: 'row',
            marginLeft: scale(0),
            width:'100%',
           
          
            borderBottomColor:'#transparent',
          
    
        }}>
                <Image source={url} resizeMode='contain' style={{ marginBottom:verticalScale(10),marginLeft:scale(70),height:verticalScale(25),width:verticalScale(25)}}/>
                <TextInput    keyboardType='email-address' style={{
        width:'100%',
        textAlign:'left',
        alignSelf:'center',
        alignItems: 'center',
        fontSize:verticalScale(16),
        color:'gray',
        marginLeft: scale(10),
        padding:0,
        fontFamily:'Avenir-Book',
        marginBottom:verticalScale(10),
        borderBottomColor:'transparent',
       

    }} placeholder={placeholder}
                placeholderTextColor='gray'
       underlineColorAndroid='transparent'
       keyboardType='email-address'

       //autoComplete={false}
      // autoCorrect={false}
      
       onChangeText = {onChange}
       value={value}
    //    onSubmitEditing={() => { this.secondTextInput.focus(); }}
    //    blurOnSubmit={false}
       />
      </ImageBackground>)
  };
 
  
  export var CommonHeader = (name,url,cls) => {
    return (
      <View style={[ComplaintStyles.SubmitButtonStyle,
{width:'100%',marginTop:0,borderRadius:0,height:verticalScale(70),flexDirection:'row',justifyContent:'space-between',paddingHorizontal:scale(20)}]}>
<View style={{flexDirection:'row',alignItems:'center'}}>
<Image source={url} style={{height:scale(60),width:scale(60)}}/>
<Text style={{fontWeight:'600',fontSize:scale(17),color:'#FFF',marginLeft:scale(20)}}>{name}</Text></View>
<Text style={{fontWeight:'600',fontSize:scale(17),color:'#FFF'}}>{cls}</Text>
      </View>

       )
  };
 
 