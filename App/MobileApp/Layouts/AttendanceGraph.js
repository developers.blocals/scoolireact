import React, { Component } from 'react'
import { Text, View,Dimensions } from 'react-native'
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from 'react-native-chart-kit'
import { verticalScale, scale } from '../../Common/utils/scale';
export default class AttendanceGraph extends Component {
    render() {
        return (
            <View style={{flex:1,backgroundColor:'#fff'}}>
              <View style={{backgroundColor:'lightgray',padding:scale(10)}}>
                <View style={{justifyContent:'space-around',alignContent:'center',height:verticalScale(50),marginLeft:scale(50)}}>
            <Text style={{fontSize:scale(18),}}>Ajeet Kumar</Text>
            <Text style={{fontSize:scale(13),color:'#777'}}>History Lecturer</Text></View>

            <LineChart
              data={{
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                datasets: [{
                  data: [0,5,10,15,20,25,30 ]
                }]
              }}
              width={scale(365)} // from react-native
              height={verticalScale(280)}
              //yAxisLabel={'$'}
              chartConfig={{
                backgroundColor: '#cfcfcf',
                backgroundGradientFrom: 'lightgray',
                backgroundGradientTo: 'lightgray',
                decimalPlaces: 0, // optional, defaults to 2dp
               color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                style: {
                  borderRadius: 1
                }
              }}
             // bezier
              style={{
                marginTop:verticalScale(10),
               //marginEnd:scale(25),
               alignSelf:'flex-end'
              }}
            />
            <View style={{flexDirection:'row',justifyContent:'space-around'}}>
              <View style={{alignItems:'center'}}>
                <Text style={{fontSize:scale(30),color:'#777'}}>162</Text><Text style={{color:'#777'}}>Total Days</Text>
              </View>
              <View style={{alignItems:'center'}}>
                <Text style={{fontSize:scale(30),color:'#777'}}>132</Text><Text style={{color:'#777'}}>Present</Text>
              </View>
              <View style={{alignItems:'center'}}>
                <Text style={{fontSize:scale(30),color:'#777'}}>30</Text><Text style={{color:'#777'}}>Not Present</Text>
              </View>
            </View>
          </View>
          <Text style={{color:'dimgray',margin:scale(18)}}>*This Attendance will be updated on monthly basis as 
            per student records
          </Text>
          </View>
        )
    }
}
