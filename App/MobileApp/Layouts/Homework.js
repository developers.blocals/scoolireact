import React, { Component } from 'react'
import { Text, View ,ImageBackground,TextInput,TouchableOpacity,Image,ScrollView,StyleSheet} from 'react-native'
import { ComplaintStyles } from '../../Common/Styles/ComplaintStyles';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import { Dropdown } from "react-native-material-dropdown";
import Colors from '../../Common/Colors';
import { SafeAreaView } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';

export default class Homework extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filePath: "",
    };
  }
  chooseFile = () => {
    var options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
 
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
       // let source = response;
        let source = { uri: response.uri };
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          filePath: source,
        });
      }
    });
  };

    render() {
        let ClassList = [{
            value: '1st',
          }, {
            value: '2nd',
          }, {
            value: '3rd',
          }];

let SubjectList = [{
    value: 'Maths',
  }, {
    value: 'Chemistry',
  }, {
    value: 'History',
  }];



        return (
            <SafeAreaView style={{flex:1}}>
            <ScrollView>
                <View style={{margin:scale(10)}}>

    <Text style={ComplaintStyles.text}>Select Class</Text>
<ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
    alignItems: 'center',
    height:verticalScale(60),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'}}>
       <Dropdown
              textColor={"gray"}
              placeholder={"Select"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20)
              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={ClassList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>
<Text style={ComplaintStyles.text}>Select Subject</Text>
<ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
    alignItems: 'center',
    height:verticalScale(60),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    >
       <Dropdown
              textColor={"gray"}
              placeholder={"Select"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20)
              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={SubjectList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>

<Text style={ComplaintStyles.text}>Upload Image</Text>
<View  style={styles.imageBack}>
<View style={styles.imageBackDash}>
<TouchableOpacity onPress={this.chooseFile} >
{this.state.filePath == "" ?
<View style={{alignItems:'center'}}>
<Image resizeMode="contain" source={require('../Images/IssuesImages/attach.png')} style={{height:verticalScale(40),width:scale(40)}}/>
<Text style={{color:'dimgray'}}>Upload an Image</Text></View> 
:
<Image resizeMode="cover" source={this.state.filePath} style={styles.image}/>
}
</TouchableOpacity>

         </View>
         </View>

<Text style={ComplaintStyles.text}><Text>Add a Note</Text><Text style={{fontSize:scale(15),fontWeight:'400'}}>(not mandatory)</Text> </Text>
<TextInput keyboardType='email-address' 
            multiline = {true}
            style={[ComplaintStyles.textInputStyle,{height:verticalScale(80),width:'95%',alignSelf:'center',textAlignVertical: 'top'}]} 
            selectionColor={"gray"}
             placeholder={'Type a note here'}
             placeholderTextColor={'gray'}
              underlineColorAndroid='transparent'
              keyboardType='email-address'

            // onChangeText = {onChange}

            //    onSubmitEditing={() => { this.secondTextInput.focus(); }}
            />
            <View style={{marginTop:verticalScale(5)}}> 

            <TouchableOpacity  onPress={() =>this.props.navigation.navigate("Issues")}
              style={ComplaintStyles.SubmitButtonStyle}>
          
                <Text style={ComplaintStyles.SignInTextStyle}>Done</Text>
              </TouchableOpacity>
              </View>
            </View>
            </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  image:{
    height:verticalScale(90),
    width:scale(275),
    borderRadius:scale(15)
   // justifyContent: "center",
    //alignItems: "center",
  },
  imageBack:{
      width: '90%',
  height:verticalScale(120),
  borderRadius:scale(15),elevation:3,
  backgroundColor:Colors.color.card,
  alignSelf:'center',
  padding: scale(10),alignItems:'center',
  marginVertical: verticalScale(10)},
imageBackDash:{
  borderStyle:'dashed', width: '100%',
height:verticalScale(100),borderColor:'gray',
borderWidth:2,alignItems:'center',justifyContent:'center',
borderRadius:scale(15),
}
});