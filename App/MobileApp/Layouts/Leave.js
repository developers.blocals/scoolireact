import React, { Component } from 'react'
import { Text, View,FlatList ,StyleSheet,Modal,TouchableOpacity,StatusBar} from 'react-native'
import Colors from '../../Common/Colors';
import { ListItem ,CheckBox,Icon} from 'react-native-elements';
import { verticalScale, scale } from '../../Common/utils/scale';
import { SafeAreaView } from 'react-navigation';
import { ComplaintStyles } from '../../Common/Styles/ComplaintStyles';
import { CommonHeader } from '../../Components/EditText';

const list = [
    {
      name: 'Akshit Sethi',
      subtitle: 'Give leave for five to Akshit because of wedding function is'+
                 'held on 20-feb to 23-feb so please give two day leave to Akshit.'
    },
    {
        name: 'Akshit Sethi',
        subtitle: 'Give leave for five to Akshit because of wedding function is'+
                   'held on 20-feb to 23-feb so please give two days leave to Akshit.'
      },
      {
        name: 'Akshit Sethi',
        subtitle: 'Give leave for five to Akshit because of wedding function is'+
                   'held on 20-feb to 23-feb so please give two days leave Akshit.'
      },
      {
        name: 'Akshit Sethi',
        subtitle: 'Give leave for five to Akshit because of wedding function is'+
                   'held on 20-feb to 23-feb so please give three days leave to Akshit.'
      },]

export default class Leave extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modalData:'',
      modalHeading:'',
      showIndicator: true,
      modalVisible: false,
    }
  }


  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
    keyExtractor = ( index) => index.toString()
    renderItem = ({ item,index }) => (
      <ListItem
     //contentContainerStyle={{marginBottom:15}}
        title={(index+1)+('. ') + ( item.name)}
        titleStyle={styles.title}
        subtitleProps={{numberOfLines:2}}
        bottomDivider
        subtitle={
            <View style={styles.subtitleView}>
              <Text numberOfLines={2} style={styles.subtitle}>{item.subtitle}</Text>
      
              <Text style={{marginTop:5,fontSize:scale(16)}}><Text>Requested by: </Text><Text style={styles.subtitle}>Shweta Sethi</Text></Text>
              <Text style={{marginTop:5,fontSize:scale(16)}}><Text>From: </Text><Text style={styles.subtitle}>09-03-2019</Text></Text>
                <Text style={{marginTop:5,fontSize:scale(16)}}><Text>To: </Text><Text style={styles.subtitle}>09-03-2019</Text></Text>
            </View>
          }

        badge={{ value: 'Show More', textStyle: {color:'#FFF',fontSize:scale(14) }, badgeStyle:{borderRadius:20,width:scale(110),height:scale(38),backgroundColor:Colors.color.themeRed}, onPress:()=>[this.setState({modalHeading:item.name,modalData:item.subtitle}),this.setModalVisible(true)] ,  status:"error" ,
        containerStyle: {position:'absolute',bottom:scale(10),right:scale(12)} }}
      />
    )

    render() {
        return (
            <View style={{flex:1,backgroundColor:'#FFF'}}>
               <StatusBar
          barStyle="light-content"
          backgroundColor={Colors.color.themeYellow}
        />
         <FlatList
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={this.renderItem}

    />
      <Modal  
          animationType="slide"
         // transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}>
            <SafeAreaView style={{flex:1,backgroundColor:Colors.color.themeYellow}}>
            <View style={styles.modal}>
 <TouchableOpacity
              hitSlop={{ top: 1, bottom: 15, left: 15, right: 1 }}
              onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
              <Icon type="ionicon" name="md-arrow-dropdown-circle" color={Colors.color.themeRed} size={scale(40)}/>
            </TouchableOpacity>
            {CommonHeader ('Akshit Sethi', require('../Images/Drawer/child.png'), '10th "A"')}
            <View style={{margin:scale(20)}}>
            <View style={{justifyContent:'space-between',flexDirection:'row',marginBottom:scale(18)}}>
            <Text style={{fontSize:scale(16)}}>Requested by: </Text>
            <Text style={styles.subtitle}>Shweta Sethi</Text>
            </View>
            <View style={{justifyContent:'space-between',flexDirection:'row',marginBottom:scale(18)}}>
             <Text style={{fontSize:scale(16)}}>From: </Text><Text style={styles.subtitle}>09-03-2019</Text></View>
            <View style={{justifyContent:'space-between',flexDirection:'row',marginBottom:scale(18)}}>
           <Text style={{fontSize:scale(16)}}>To: </Text><Text style={styles.subtitle}>09-03-2019</Text></View>
<Text style={{marginBottom:scale(18),fontSize:scale(16)}}>Message</Text>
<Text style={styles.message}>
{this.state.modalData}
</Text>

<View style={{flexDirection:'row',justifyContent:'space-between',position:'relative',top:verticalScale(250)}}>
<TouchableOpacity  onPress={() =>console.log('Approve')}
              style={[ComplaintStyles.SubmitButtonStyle,{width:'45%'}]}>
          
                <Text style={ComplaintStyles.SignInTextStyle}>Approve</Text>
              </TouchableOpacity>
              <TouchableOpacity  onPress={() =>console.log('Approve')}
              style={[ComplaintStyles.SubmitButtonStyle,{width:'45%'}]}>
          
                <Text style={ComplaintStyles.SignInTextStyle}>Denied</Text>
              </TouchableOpacity>
              </View>
</View>
</View>
</SafeAreaView>
</Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
      color:Colors.color.themeRed,

    },
    subtitle:{
        color:'gray',
        fontSize:scale(16)
    },
    subtitleView: {
        paddingTop: 5
      },
      message:{
        backgroundColor:Colors.color.card,
        padding:scale(15),borderRadius:5,
        color:'gray',fontSize:scale(16),
        maxHeight:verticalScale(300)
      },
      modal: {
        flex: 1,
        backgroundColor:"#FFF",
      },
  })