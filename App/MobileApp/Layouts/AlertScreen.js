import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList } from 'react-native';
import { ListItem ,CheckBox} from 'react-native-elements';
import Colors from "../../Common/Colors";
import { SafeAreaView } from 'react-navigation';
import { scale, verticalScale } from '../../Common/utils/scale';
import Moment from 'moment';

const list = [
    {
      name: 'Akshit Sethi',
      subtitle: 'Give leave for five to Akshit because of wedding function is'+
                 'held on 20-feb to 23-feb so please give two days leave to Akshit.',
                 date:"22-feb-2019"

    },
    {
        name: 'Akshit Sethi',
        subtitle: 'Give leave for five to Akshit because of wedding function is'+
                   'held on 20-feb to 23-feb so please give two days leave to Akshit.',
                   date:"22-feb-2019"
      },
      {
        name: 'Akshit Sethi',
        subtitle: 'Give leave for five to Akshit because of wedding function is'+
                   'held on 20-feb to 23-feb so please give two days leave to Akshit.',
                   date:"22-feb-2019"

      },
      {
        name: 'Akshit Sethi',
        subtitle: 'Give leave for five to Akshit because of wedding function is'+
                   'held on 20-feb to 23-feb so please give two days leave to Akshit.',
                   date:"22-feb-2019"

      },]


export default class AlertScreen extends Component {
    keyExtractor = ( index) => index.toString()
    renderItem = ({ item,index }) => (
      <ListItem
        title={(item.name)}
        titleStyle={styles.title}
        bottomDivider
        subtitle={<View style={{flexGrow:1}}>
            <Text style={{color:'gray',alignSelf:"flex-end",marginTop:verticalScale(-15),marginBottom:verticalScale(8)}}>{Moment(item.date).format("DD-MMM-YYYY")}</Text>
            <Text style={{color:'gray'}}>{item.subtitle}</Text>
        </View>
            }
      />
    )



    render() {

        return (
<SafeAreaView style={{flex:1,backgroundColor:Colors.color.themeYellow}}>
            <View style={styles.container}>
                
            <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />
         <FlatList
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={this.renderItem}

    />
            </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        color:Colors.color.themeRed
      },
      subtitle:{
          color:'gray',
          
      },
});