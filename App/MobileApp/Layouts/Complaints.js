import React, { Component } from 'react';
import { YellowBox, Platform, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ImageBackground } from 'react-native';
import { Dropdown } from "react-native-material-dropdown";
import { StackActions, NavigationActions } from 'react-navigation';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import { EditText } from '../../Components/EditText';
import { TextInput } from 'react-native-gesture-handler';
import { ComplaintStyles } from '../../Common/Styles/ComplaintStyles';


export default class Complaints extends Component {
  static navigationOptions = {
    title: 'Complaint',
  };
  componentDidMount() {
    console.disableYellowBox = true;

  }


  render() {
    let ComplaintList = [{
      value: 'Complaint1',
    }, {
      value: 'Coplaint2',
    }, {
      value: 'Others',
    }];
    return (

      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
        />

        <View style={{margin:scale(20)}}>

          <Text style={ComplaintStyles.text}>Select Complaint Topic</Text>
         
          <ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
   alignSelf:"center",
    alignItems: 'center',
    height:moderateScale(68),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    >
       <Dropdown
              textColor={"gray"}
              placeholder={"Select complaint topic"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20),

              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={ComplaintList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>
          <Text style={ComplaintStyles.text}>Type your message</Text>
         
            <TextInput keyboardType='email-address' 
            multiline = {true}
            style={[ComplaintStyles.textInputStyle,{height:verticalScale(200),textAlignVertical: 'top'}]} 
            selectionColor={"gray"}

              underlineColorAndroid='transparent'
              keyboardType='email-address'

            // onChangeText = {onChange}

            //    onSubmitEditing={() => { this.secondTextInput.focus(); }}
            />

          {/* <View style={{height:'35%',justifyContent:'flex-end'}}>  */}

          <TouchableOpacity  onPress={() =>this.props.navigation.navigate("Issues")}
            style={ComplaintStyles.SubmitButtonStyle}>
        
              <Text style={ComplaintStyles.SignInTextStyle}>Submit</Text>
            </TouchableOpacity>
            {/* </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
   // justifyContent:"center",
  },
  
});