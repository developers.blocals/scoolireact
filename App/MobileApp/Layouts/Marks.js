import React, { Component } from 'react'
import { Text, View ,ImageBackground,StatusBar,StyleSheet,FlatList} from 'react-native'
import { CommonHeader } from '../../Components/EditText'
import { Dropdown } from "react-native-material-dropdown";
import { ComplaintStyles } from '../../Common/Styles/ComplaintStyles';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import Colors from '../../Common/Colors';
const DATA = [
  {
    title: 'English',
    data: [{"key":'Oral',"marks":"Theory","status":"Total"},
    {"key": '05/25',"marks":"05/25","status":"05/25"},
    {"key": 'Pass',"marks":"Pass","status":"Pass"}],
  },
  {
    title: 'Hindi',
    data: [{"key":'Oral',"marks":"Theory","status":"Total"},
    {"key": '05/25',"marks":"05/25","status":"05/25"},
    {"key": 'Pass',"marks":"Pass","status":"Pass"}],  },
  {
    title: 'Maths',
    data: [{"key":'Oral',"marks":"Theory","status":"Total"},
    {"key": '05/25',"marks":"05/25","status":"05/25"},
    {"key": 'Pass',"marks":"Pass","status":"Pass"}], },
  {
    title: 'Science',
    data: [{"key":'Oral',"marks":"Theory","status":"Total"},
    {"key": '05/25',"marks":"05/25","status":"05/25"},
    {"key": 'Pass',"marks":"Pass","status":"Pass"}],  },
];



export default class Marks extends Component {
    render() {
        let ExamtList = [{
            value: 'Quaterly',
          }, {
            value: 'Halfyearly',
          }, {
            value: 'Annual',
          }];
        return (
<View style={styles.container}>
      
        {CommonHeader ('Akshit Sethi', require('../Images/Drawer/child.png'),"")}

            <Text style={{color:'gray',margin:scale(10),marginStart:scale(35),fontSize:scale(18)}}>Select exam type</Text>
         
         <ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
style={{
    alignSelf:'center',
   alignItems: 'center',
   height:moderateScale(68),
   flexDirection: 'row',
   width:scale(340),
   borderBottomColor:'#transparent',
justifyContent:'center'
}}   >
      <Dropdown
             textColor={"gray"}
             placeholder={"Select exam type"}
             placeholderTextColor={"gray"}
             inputContainerStyle={{
               borderBottomColor: "transparent",
               width:scale(280),
               justifyContent:"center",
               height:verticalScale(20)
             }}
             pickerStyle={{
                 alignSelf:'center',
               backgroundColor: Colors.color.card,
               width: scale(260),
             // marginLeft: wp("1.5%")
             }}
             itemPadding={8}
             dropdownOffset={{ top: 12, left: 0 }}
           //  rippleInsets={{ top: 0, bottom: 0 }}
             data={ExamtList}
             onChangeText={this.onChangeText2}
           />


         </ImageBackground>

         <View style={{flex:1,alignSelf:'center', width:'88%',borderTopLeftRadius:scale(15),borderTopRightRadius:scale(15),marginTop:scale(15),
         backgroundColor:Colors.color.card}}>
         <View style={{justifyContent:'space-around',backgroundColor:Colors.color.themeYellow,flexDirection:'row',padding:scale(15),
        borderTopLeftRadius:scale(15),borderTopRightRadius:scale(15)}}>
           <Text style={{color:'#FFF',fontSize:scale(17)}}>Subject</Text>
           <Text style={{color:'#FFF',fontSize:scale(17)}}>Marks</Text>
           <Text style={{color:'#FFF',fontSize:scale(17)}}>Status</Text>
         </View>
         <FlatList
        data={DATA}
        style={{padding:scale(13)}}
        contentContainerStyle={{paddingBottom:verticalScale(50)}}
        renderItem={({ item ,index}) => 
        <View>
        <Text style={{color:Colors.color.themeRed,marginVertical:scale(10),fontSize:scale(16)}}>{(index+1)+". "+item.title}</Text>
        <FlatList
        scrollEnabled={false}
        contentContainerStyle={{justifyContent:'space-around',flex:1}}
        style={{backgroundColor:'#FFF',width:'100%',borderRadius:scale(10),alignSelf:'center',padding:scale(15)}}
        data={item.data}
        horizontal
        ItemSeparatorComponent={()=><View style={{height:verticalScale(55),alignSelf:'center',width:1.5,backgroundColor:"gray",marginLeft:scale(25)}}/>}
        renderItem={({ item }) => 
        <View >
        <Text style={{color:'gray',flex:1,textAlign:'center'}}>{item.key}</Text>
        <Text style={{color:'gray',flex:1,textAlign:'center'}}>{item.marks}</Text>
        <Text style={{color:'gray',flex:1,textAlign:'center'}}>{item.status}</Text>
        </View>}
        keyExtractor={item => item.key}
      />
        </View>}
        keyExtractor={item => item.title}
      />
        
         </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    // alignContent:"center",
    },
   
  });
  