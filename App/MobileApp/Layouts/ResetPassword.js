import React, { Component } from 'react';
import {YellowBox, Platform, StyleSheet, Text, View, Image,StatusBar,TouchableOpacity,ImageBackground } from 'react-native';
import Colors from "../../Common/Colors";
import { LoginStyles } from '../../Common/Styles/LoginStyles';
import { scale, verticalScale,moderateScale } from '../../Common/utils/scale';
import { EditText } from '../../Components/EditText';
import { SafeAreaView } from 'react-navigation';

export default class ResetPassword extends Component {
constructor(props){
    super(props),
    this.state={

    }
}
Submit(){
    this.props.navigation.navigate("PasswordChange")
  } 

  render() {
 
    return (
      <SafeAreaView style={{flex:1}}>
        <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />
      <View style={styles.container}>
        <Image resizeMode={"center"} style={LoginStyles.logoStyles}
            source={require('../Images/LogoImages/logo.png')} />
            <View style={{flex:1,justifyContent:"space-around"}}>
         <Text style={{color:"#FF3662",fontSize:moderateScale(30),marginStart:"10%",fontWeight:"bold"}}>
              Forget {'\n'}Password</Text>
            <View>
             {EditText( 'Enter Password',require('../Images/LoginImages/lock.png'),null,null)} 
             {EditText( 'Confirm Password',require('../Images/LoginImages/lock.png'),null,null)} 
             </View>     
                      <TouchableOpacity  onPress={()=>this.Submit()}
                        style={[LoginStyles.SubmitButtonStyle,{width:"56%",height:moderateScale(44)}]}>
                            <Text style={LoginStyles.SignInTextStyle}>Submit</Text>
                      </TouchableOpacity>
        </View>
      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  }
});