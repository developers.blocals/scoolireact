import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList,ActivityIndicator } from 'react-native';
import { ListItem ,Image, Overlay} from 'react-native-elements';
import Colors from "../../Common/Colors";
import { SafeAreaView } from 'react-navigation';
import { scale, verticalScale } from '../../Common/utils/scale';
import Moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';

const list = [
    {
      name: 'Unit Test Notification',
      subtitle: 'Unit test will be '+
                 'held on 20-feb to 23-feb for class 9th, 10th, 11th and 12th.',
                 date:"23-feb-2019"

    },
    {
        name: 'Sport Day Event',
        image: [
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2yJyeTXEpTzhp07yMnOr27onNRfm0U3cFCmjDqsdhBCAPRXX36A'},
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzBIT780-xhbcboXnSz5KJN9DvNomFsplFiA7OOmYWtVegcPC_'},
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2yJyeTXEpTzhp07yMnOr27onNRfm0U3cFCmjDqsdhBCAPRXX36A'},
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzBIT780-xhbcboXnSz5KJN9DvNomFsplFiA7OOmYWtVegcPC_'},
               {uri:'https://www.signupgenius.com/cms/images/school/iStock_000060174500_60x40.jpg'},

             ],
                   date:"22-feb-2019"
      },
      {
        name: 'Unit Test Notification',
        subtitle: 'Unit test will be '+
        'held on 20-feb to 23-feb for class 9th, 10th, 11th and 12th.',
                   date:"21-feb-2019"

      },
      {
        name: 'Sport Day Event',
        image: [
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2yJyeTXEpTzhp07yMnOr27onNRfm0U3cFCmjDqsdhBCAPRXX36A'},
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2yJyeTXEpTzhp07yMnOr27onNRfm0U3cFCmjDqsdhBCAPRXX36A'},
            {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzBIT780-xhbcboXnSz5KJN9DvNomFsplFiA7OOmYWtVegcPC_'},
               {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzBIT780-xhbcboXnSz5KJN9DvNomFsplFiA7OOmYWtVegcPC_'},
               {uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2yJyeTXEpTzhp07yMnOr27onNRfm0U3cFCmjDqsdhBCAPRXX36A'},

             ],
                   date:"20-feb-2019"

      },]

export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isVisible: false,
          overlayTitle:'',
          overlayDate:'',
          overlayData:[],
          overlayIndex:''
        };
      }

    keyExtractor = ( index) => index.toString()

    renderItem = ({ item,index }) => (
      <ListItem
        title={(item.name)}
        titleStyle={styles.title}
        bottomDivider
        subtitle={<View style={{flexGrow:1}}>
            <Text style={{color:'gray',alignSelf:"flex-end",marginTop:verticalScale(-15),marginBottom:verticalScale(8)}}>{Moment(item.date).format("DD-MMM-YYYY")}</Text>
            {item.subtitle == undefined ?
       <FlatList
       keyExtractor={this.keyExtractor}
       data={item.image}
       horizontal
       renderItem={({ item:i ,index}) => (
        <View>
            <TouchableOpacity onPress={()=>this.setState({isVisible:true,overlayTitle:item.name,overlayDate:item.date,overlayData:item.image,overlayIndex:index})}>
            <Image source={{uri:i.uri}} 
            style={{height:scale(100),width:scale(100),borderRadius:20,margin:10}}
            PlaceholderContent={<ActivityIndicator />}
            />
            </TouchableOpacity>
        </View>
    )}
     />
            :
            <Text style={{color:'gray'}}>{item.subtitle}</Text>}

        </View>
            }
      />
    )



    render() {

        return (
<SafeAreaView style={{flex:1,backgroundColor:Colors.color.themeYellow}}>
            <View style={styles.container}>
                
            <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />
         <FlatList
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={this.renderItem}

    />
            </View>
            <Overlay isVisible={this.state.isVisible}
             overlayStyle={{position: 'absolute',bottom:0}}
            onBackdropPress={() => this.setState({ isVisible: false })}
            //windowBackgroundColor="rgba(255, 255, 255, .5)"
             width="100%"
            height="50%"
            >
              <View>
              <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',margin:scale(10)}}>
          <Text style={[styles.title,{fontSize:scale(20)}]}>{this.state.overlayTitle}</Text>
          <Text style={{color:'gray',fontSize:scale(17),fontWeight:500}}>{Moment(this.state.overlayDate).format("DD-MMM-YYYY")}</Text>
          </View>
       <View style={{backgroundColor:'#aaa',height:1,width:'100%'}}/>
      
       <FlatList
       ref={(ref) => { this.flatListRef = ref; }}
       keyExtractor={this.keyExtractor}
       initialScrollIndex={this.state.overlayIndex}
          onScrollToIndexFailed={()=>{}}
       data={this.state.overlayData}
       horizontal
       renderItem={({ item,index}) => (
        <View>
            <Image source={{uri:item.uri}} 
            style={{height:scale(300),width:scale(300),borderRadius:30,margin:scale(20)}}
            PlaceholderContent={<ActivityIndicator />}
            />
        </View>
    )}
     />
          </View>
        </Overlay>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        color:Colors.color.themeRed
      },
      subtitle:{
          color:'gray',
          
      },
});