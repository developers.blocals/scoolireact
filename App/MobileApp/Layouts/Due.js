import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, FlatList } from 'react-native';


import { scale, verticalScale } from '../../Common/utils/scale';

export default class Due extends Component {

    componentDidMount() {
        console.disableYellowBox = true;

    }


    render() {

        return (

            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.color.themeYellow}
                    barStyle="dark-content"
                />
                <View style={{ flexDirection: 'row', height: verticalScale(64), backgroundColor: 'yellow', width: '100%' }}>
                    <Image style={{ width: scale(24), alignSelf: 'center', marginLeft: scale(10), height: verticalScale(24) }} source={require('../Images/AlertScreenImages/back_arrow.png')} />
                    <Text style={{ alignSelf: 'center', marginLeft: scale(125), color: '#fff', fontSize: scale(20) }}>Alert</Text>
                </View>

                <FlatList

                    data={[{ key: 'a' }, { key: 'b' }]}
                    renderItem={({ item }) => {
                        return (
                            <View>
                                <View style={{ flexDirection: 'row', marginLeft: scale(10) }}>
                                    <Text style={{ marginLeft: scale(10), alignSelf: 'center', flex: .7 }}>{item.key}</Text>
                                    <Text style={{ alignSelf: 'center', flex: .3 }}>{item.key}</Text>
                                </View>
                                <Text style={{ marginLeft: scale(20) }}>{item.key}</Text>
                            </View>

                        );
                    }}
                    keyExtractor={(item, index) => index}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    }
});