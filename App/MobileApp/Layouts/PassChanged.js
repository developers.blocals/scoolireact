import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { LoginStyles } from "../../Common/Styles/LoginStyles";
import { PassChangeStyles } from "../../Common/Styles/PassChangeStyles";

export default class PassChanged extends Component {
  componentDidMount() {
    console.disableYellowBox = true;
  }
  onClick = () => {
    this.props.navigation.navigate("Login");
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={Colors.color.themeYellow} barStyle="dark-content" />

        <View style={{ height: "100%" }}>
          <Image
            resizeMode={"center"}
            style={PassChangeStyles.logoStyles}
            source={require("../Images/LogoImages/logo.png")}
          />
          <Image
            resizeMode={"center"}
            style={PassChangeStyles.successStyles}
            source={require("../Images/LogoImages/success.png")}
          />

          <TouchableOpacity
            style={{ width: "100%" }}
            onPress={() => {
              this.onClick();
            }}
          >
            <ImageBackground
              resizeMode="contain"
              source={require("../Images/LoginImages/btn_bg.png")}
              style={LoginStyles.SubmitButtonStyle}
            >
              <Text style={LoginStyles.SignInTextStyle}>LOGIN</Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});
