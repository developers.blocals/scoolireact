import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image, StatusBar, FlatList, ImageBackground } from 'react-native';


import { scale, verticalScale } from '../../Common/utils/scale';
import Colors from '../../Common/Colors';

export default class StudentTt extends Component {

    componentDidMount() {
        console.disableYellowBox = true;

    }


    render() {

        return (
<SafeAreaView style={{flex:1,backgroundColor:Colors.color.themeYellow}}>

            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.color.themeYellow}
                    barStyle="light-content"
                />

                <FlatList

                    data={[{ key: '12:00 PM' , value:'English'}, { key: '01:00 PM' ,value:'History'},{ key: '02:00 PM' ,value:'Science'},
                    { key: '12:00 PM' , value:'English'}, { key: '01:00 PM' ,value:'History'},{ key: '02:00 PM' ,value:'Science'}]}
                    renderItem={({ item }) => {
                        return (
                            <View>
                                    <View style={styles.backgroundd}>
                                        <Text style={{ color:'#444',fontSize:scale(20)}}>{item.key}</Text>
                                        <Image style={{ tintColor:'#444',width: scale(30), height: verticalScale(15)}} resizeMode="contain" source={require('../Images/TtImages/arrow.png')} />
                                        <Text style={{ color:'#444',fontSize:scale(20)}}>{item.value}</Text>
                                    </View>
                            </View>

                        );
                    }}
                    keyExtractor={(item, index) => index}
                />

            </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    backgroundd:{
        width: '90%',justifyContent:'space-around',
         alignItems:'center',flexDirection: 'row',
          height: verticalScale(80),
          alignSelf:'center',
          backgroundColor:Colors.color.card,
          marginVertical:verticalScale(10),
          borderRadius:verticalScale(10),
          elevation:3
    }
});