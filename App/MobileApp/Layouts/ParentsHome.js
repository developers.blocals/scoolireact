import React, { Component } from 'react';
import { SafeAreaView, FlatList, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ImageBackground, Platform } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import Slideshow from 'react-native-slideshow';
import { DrawerActions } from 'react-navigation-drawer';

import { sliderWidth, SliderEntry, itemWidth } from '../../Common/Styles/SliderEntry';
import Colors from "../../Common/Colors";

const SLIDER_1_FIRST_ITEM = 1;
export default class ParentHome extends Component {

  constructor(props) {
    super(props);

    this.state = {
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
      position: 1,
      interval: null,
      ENTRIES1: [
        {
          title: 'Gagan Sethi',
          subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
          illustration: 'https://i.imgur.com/UYiroysl.jpg'
        },
        {
          title: 'Ankit Singh',
          subtitle: 'Lorem ipsum dolor sit amet',
          illustration: 'https://i.imgur.com/UPrs1EWl.jpg'
        },
        {
          title: 'Abhishek Singh',
          subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
          illustration: 'https://i.imgur.com/MABUbpDl.jpg'
        },
        {
          title: 'Amit',
          subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
          illustration: 'https://i.imgur.com/KZsmUi2l.jpg'
        },
        {
          title: 'Tushar',
          subtitle: 'Lorem ipsum dolor sit amet',
          illustration: 'https://i.imgur.com/2nCt3Sbl.jpg'
        },
        {
          title: 'Gulati',
          subtitle: 'Lorem ipsum dolor sit amet',
          illustration: 'https://i.imgur.com/lceHsT6l.jpg'
        }
      ],
      dataSource: [
        {
          // title: 'Title 1',
          // caption: 'Caption 1',
          url: 'http://placeimg.com/640/480/arch',
        }, {
          // title: 'Title 2',
          // caption: 'Caption 2',
          url: 'http://placeimg.com/640/480/arch',
        }, {
          // title: 'Title 3',
          // caption: 'Caption 3',
          url: 'http://placeimg.com/640/480/arch',
        },
      ],
      loading: false,
      data: [
        {
          imageUrl: require("../Images/ParentMenuImages/bus.png"),
          title: "Live Bus Tracking"
        },

        {
          imageUrl: require("../Images/ParentMenuImages/homework.png"),
          title: "Home work"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/attendance.png"),
          title: "Attendance"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/leave.png"),
          title: "Leave"
        },

        {
          imageUrl: require("../Images/ParentMenuImages/note.png"),
          title: "Notes"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/alerts.png"),
          title: "Alerts"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/graph.png"),
          title: "Time Table"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/due.png"),
          title: "Complaints"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/chat.png"),
          title: "Chat"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/marks.png"),
          title: "Marks"
        },
        {
          imageUrl: require("../Images/ParentMenuImages/leave.png"),
          title: "Leave Student"
        }
      ],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
    };
  }
  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        });
      }, 2000)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }
  componentDidMount() {
    console.disableYellowBox = true;

  }
  listPress(i) {
    if (i == 0) {
      this.props.navigation.navigate('TrackBus')
    }
    else if (i == 1) {
      this.props.navigation.navigate('Homework')
    }
    else if (i == 2) {
      this.props.navigation.navigate('AttendanceGraph')
    }
    else if (i == 3) {
      this.props.navigation.navigate('Leave')
    }
    else if (i == 4) {
      this.props.navigation.navigate('Notes')
    }
    else if (i == 5) {
      this.props.navigation.navigate('AlertScreen')
    }
    else if (i == 6) {
      this.props.navigation.navigate('TimeTable')
    }
    else if (i == 7) {
      this.props.navigation.navigate('Complaints')
    }
    else if (i == 8) {
      this.props.navigation.navigate('Chat')
    }
    else if (i == 9) {
      this.props.navigation.navigate('Marks')
    }
    else if (i == 10) {
      this.props.navigation.navigate('LeaveStudent')
    }

  }

  _renderItemWithParallax({ item, index }) {
    return (
      <View style={SliderEntry.slider} >
        <View style={{ height: '100%',marginBottom:verticalScale(10), justifyContent: "space-evenly",paddingBottom:verticalScale(30),alignItems: "center", width: scale(250), backgroundColor: Colors.color.card, 
        borderTopRightRadius: 18,borderTopLeftRadius:18}}>
          <Image style={{ width:scale(130), height: verticalScale(130) }} source={require('../Images/Drawer/child.png')} />
          <Text numberOfLines={1} style={{ fontSize: scale(20), color: "gray", fontWeight: "600" }}>{item.title}</Text>
          <View style={{ width: "80%" }}>
            <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
              <Text style={{ fontSize: scale(14), color: "dimgray" }}>Class</Text>
              <Text>:</Text>
              <Text style={{ fontSize: scale(14), color: "dimgray" }}>6th 'A'</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: verticalScale(10), justifyContent: "space-around" }}>
              <Text style={{ fontSize: scale(14), color: "dimgray" }}>Roll No</Text>
              <Text>:</Text>
              <Text style={{ fontSize: scale(14), color: "dimgray" }}>21</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }


  render() {

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: Colors.color.themeYellow }}>
        <View style={styles.container}>
          <StatusBar
            backgroundColor={Colors.color.themeYellow}
            barStyle="light-content"
          />
          <View style={{ marginTop:verticalScale(6) }}>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
              <View style={{ marginLeft:scale(10) }}>
                <Text style={{ fontSize: scale(15), color: Colors.color.themeRed, fontWeight: "600" }}>Delhi Public School</Text>
                <Text style={{ fontSize: scale(14), color: 'gray' }}>New Delhi</Text>
              </View>
              <View>
                <TouchableOpacity onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())} hitSlop={{ top: 8, bottom: 10, left: 15, right: 18 }}>
                  <ImageBackground resizeMode="contain" style={{ height:verticalScale(35), width:scale(60), justifyContent: "center", alignItems: "flex-start", padding: scale(2) }}
                    source={require("../Images/Drawer/openDrawer.png")}>
                    <Image resizeMode="contain" style={{ height:scale(30), width: scale(30) }}
                      source={require("../Images/Drawer/avatars.png")} />
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ paddingVertical: verticalScale(5), width: '100%' }}>
              <FlatList
                horizontal
                data={this.state.data}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {
                  return (
                    <TouchableOpacity onPress={() => this.listPress(index)}>
                      <View style={{ margin: scale(7), justifyContent: "center", alignItems: "center", width: scale(70) }}>
                        <ImageBackground style={{ marginVertical:scale(5), width: scale(60), height: scale(60), justifyContent: 'center', alignItems: 'center' }}
                          source={require('../Images/ParentMenuImages/bus_bg.png')}>
                          <Image resizeMode='contain' style={{ width: scale(30), height: scale(30) }} source={item.imageUrl} />
                        </ImageBackground>
                        <Text style={{ alignSelf: "center", fontSize: scale(12), color: Colors.color.themeRed, textAlign: 'center' }}>{item.title}</Text>
                      </View>
                    </TouchableOpacity>

                  );
                }}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>



          <View style={{ width: '100%', height: verticalScale(0.7), backgroundColor: 'dimgray' }}></View>

          <Slideshow
            height={verticalScale(120)}
            containerStyle={{ width:"85%",alignSelf:'center',marginVertical:verticalScale(15),
            borderRadius: scale(16),overflow:'hidden' }}
            dataSource={this.state.dataSource}
            position={this.state.position}
            onPositionChanged={position => this.setState({ position })} />

          <Carousel
            ref={c => this._slider1Ref = c}
            data={this.state.ENTRIES1}
            renderItem={this._renderItemWithParallax}
            sliderWidth={scale(360)}
            itemWidth={scale(260)}
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0.7}
            // inactiveSlideShift={20}
            layout={'default'}
            firstItem={0}
            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  }
});