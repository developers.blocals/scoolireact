import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity} from "react-native";

import { scale, verticalScale } from "../../../Common/utils/scale";
import { Icon } from "react-native-elements";
import { IssuesStyles } from "../../../Common/Styles/IssuesStyles";
import Colors from "../../../Common/Colors";

export default class Issues extends Component {

  static navigationOptions = {
    title: "Issues"
  };
  componentDidMount() {
    console.disableYellowBox = true;
  }

  render() {
    return (
      <View style={IssuesStyles.container}>
        <StatusBar backgroundColor={Colors.color.themeYellow} barStyle="light-content" />

        <Image
          resizeMode={"center"}
          style={{
            height: scale(155),
            width: scale(125),
          }}
          source={require("../../Images/IssuesImages/msg.png")}
        />
        <View style={IssuesStyles.view}>
          <TouchableOpacity
            style={IssuesStyles.BurttonStyle}
            onPress={() => {this.props.navigation.navigate('CreateIssues')}}
          >
            <Text style={IssuesStyles.text}>Create Issue</Text>
            <Icon name="ios-arrow-forward" type="ionicon" color="#FFF" />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              IssuesStyles.BurttonStyle,
              { backgroundColor: Colors.color.themeRed }
            ]}
            onPress={() => {this.props.navigation.navigate('YourIssues')}}
          >
            <Text style={IssuesStyles.text}>Your Issue</Text>
            <Icon name="ios-arrow-forward" type="ionicon" color="#FFF" />
          </TouchableOpacity>

          <TouchableOpacity
            style={IssuesStyles.BurttonStyle}
            onPress={() => {this.props.navigation.navigate('AllIssues')}}
          >
            <Text style={IssuesStyles.text}>All Issue</Text>
            <Icon name="ios-arrow-forward" type="ionicon" color="#FFF" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


