import React, { Component } from 'react'
import { View ,FlatList} from 'react-native'
import { ListItem} from 'react-native-elements';

const list = [
    {
      name: 'Whole square of a + b'
    },
    {
        name: 'Whole square of a + b'
      },
      {
        name: 'Whole square of a + b'
      },
      {
        name: 'Whole square of a + b'
      },]

export default class AllIssues extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked:false}}

    keyExtractor = (item, index) => index.toString()

renderItem = ({ item }) => (
  <ListItem
    title={item.name}
    bottomDivider
    chevron
  />
)

    render() {
        return (
            <View style={{flex:1}}>

                <FlatList
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={this.renderItem}
    />
            </View>
        )
    }
}