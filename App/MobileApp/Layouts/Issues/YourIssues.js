import React, { Component } from 'react'
import { Text, View ,FlatList,StyleSheet} from 'react-native'
import { ListItem ,CheckBox} from 'react-native-elements';
import Colors from "../../../Common/Colors";
import { FAB } from 'react-native-paper';
import { verticalScale, scale } from '../../../Common/utils/scale';

const list = [
    {
      name: 'Whole square of a + b'
    },
    {
        name: 'Whole square of a + b'
      },
      {
        name: 'Whole square of a + b'
      },
      {
        name: 'Whole square of a + b'
      },]

export default class YourIssues extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked:false}}

    keyExtractor = (item, index) => index.toString()

renderItem = ({ item }) => (
  <ListItem
    title={item.name}
    bottomDivider
    chevron
    checkBox={{
        checked:this.state.checked,
        onPress:() => this.setState({ checked: !this.state.checked }),
        //checkedColor:'red',
        uncheckedColor:Colors.color.themeYellow}
//   <CheckBox
//               center
//               iconLeft
//               containerStyle={{ padding: 0 }}
//               iconType="font-awesome"
//               uncheckedIcon="check-square"
//               checkedIcon="square"
//               uncheckedColor={Colors.color.themeYellow}
//               checked={this.state.checked}
//               onPress={() => this.setState({ checked: !this.state.checked })}
//             />
    }
          
  />
)

    render() {
        return (
            <View style={{flex:1}}>

                <FlatList
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={this.renderItem}
    />
    <FAB
    style={styles.fab}
    icon="delete"
    onPress={() => console.log('Pressed')}
  />
            </View>
        )
    }
}
const styles = StyleSheet.create({
  fab: {
    backgroundColor:Colors.color.themeRed,
    position: 'absolute',
   // margin: sca16,
    right:scale(15),
    bottom:verticalScale(20),
  },
})