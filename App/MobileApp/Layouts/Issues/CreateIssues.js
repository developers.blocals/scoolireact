import React, { Component } from 'react'
import { Text, View ,TextInput,StatusBar,TouchableOpacity,ImageBackground,Image} from 'react-native'
import Colors from "../../../Common/Colors";
import { scale, verticalScale, moderateScale } from '../../../Common/utils/scale';
import { ComplaintStyles } from '../../../Common/Styles/ComplaintStyles';
import { EditText } from '../../../Components/EditText';


export default class CreateIssues extends Component {
    render() {
        return (
            <View style={{flex:1,backgroundColor:'#FFF',margin:scale(15)}}>
                <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
        />
                <Text style={ComplaintStyles.text }>Add Title</Text>
            {/* <ImageBackground resizeMode='stretch' source={require('../../Images/LoginImages/edittxt_bg.png')} 
 style={{
    alignItems: 'center',
    height:moderateScale(68),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    > */}
      <TextInput keyboardType='email-address' 
            style={[ComplaintStyles.textInputStyle,{height:verticalScale(50),borderRadius:25}]} 
            selectionColor={"gray"}
placeholder={'Type title here'}
placeholderTextColor={'gray'}
              underlineColorAndroid='transparent'
              keyboardType='email-address'

            // onChangeText = {onChange}

            //    onSubmitEditing={() => { this.secondTextInput.focus(); }}
            />
      {/* </ImageBackground> */}
                {/* <TextInput keyboardType='email-address' style={{
              width: '100%',
              textAlign: 'left',
              alignSelf: 'center',
              alignItems: 'center',
              fontSize: verticalScale(18),
              color: 'gray',
              marginLeft: scale(10),
              padding: 0,
              fontFamily: 'Avenir-Book',
              marginBottom: verticalScale(10),
              borderBottomColor: 'transparent',


            }} placeholder='Type title here'
              placeholderTextColor='gray'
              underlineColorAndroid='transparent'
              keyboardType='email-address'
              /> */}
           
           <Text style={ComplaintStyles.text}>Type your message</Text>
         
         <TextInput keyboardType='email-address' 
         multiline = {true}
         style={ComplaintStyles.textInputStyle} 
         selectionColor={"gray"}

           underlineColorAndroid='transparent'
           keyboardType='email-address'

         // onChangeText = {onChange}

         //    onSubmitEditing={() => { this.secondTextInput.focus(); }}
         />

<Text style={ComplaintStyles.text}>Attach File</Text>
<View
         style={{  width: '100%',
         height:verticalScale(150),
         borderRadius:scale(15),elevation:3,
         backgroundColor:Colors.color.card,
         padding: scale(15),alignItems:'center',
         marginVertical: verticalScale(10)}}>
           <View style={{borderStyle:'dashed', width: '100%',
         height:verticalScale(120),borderColor:'gray',
         borderWidth:scale(2),alignItems:'center',justifyContent:'center',
         borderRadius:scale(15),
         }}>
<Image source={require('../../Images/IssuesImages/attach.png')} style={{height:verticalScale(35),width:scale(40)}}/>
<Text style={{color:'dimgray'}}>Attach File</Text>
         </View>
         </View>
<TouchableOpacity  onPress={() =>this.props.navigation.navigate("Issues")}
            style={[ComplaintStyles.SubmitButtonStyle,{marginTop:verticalScale(40)}]}>
        
              <Text style={ComplaintStyles.SignInTextStyle}>Done</Text>
            </TouchableOpacity>


            </View>
        )
    }
}
