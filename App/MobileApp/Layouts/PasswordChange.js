import React, { Component } from 'react';
import {YellowBox, Platform, StyleSheet, Text, View, Image,StatusBar,TouchableOpacity,ImageBackground } from 'react-native';
import Colors from "../../Common/Colors";
import { LoginStyles } from '../../Common/Styles/LoginStyles';
import { scale, verticalScale,moderateScale } from '../../Common/utils/scale';
import { EditText } from '../../Components/EditText';
import { SafeAreaView } from 'react-navigation';

export default class PasswordChange extends Component {
constructor(props){
    super(props),
    this.state={

    }
}

  render() {
 
    return (
      <SafeAreaView style={{flex:1}}>
        <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />
      <View style={styles.container}>
        <Image resizeMode={"center"} style={[LoginStyles.logoStyles,{alignSelf:"flex-end",marginBottom:0,marginTop:0}]}
            source={require('../Images/LogoImages/logo.png')} />
            <View style={{flex:1,justifyContent:"space-around"}}>
            <Image resizeMode={"center"} style={[LoginStyles.logoStyles,{marginTop:0,marginBottom:0}]}
            source={require('../Images/AlertScreenImages/tick.png')} />
         <Text style={{color:Colors.color.themeRed,fontSize:moderateScale(17),marginLeft:scale(10)}}>
             <Text>Your Password has been changed successfully {'\n'}</Text>
              <Text>Please login with username and password</Text>
              </Text>
            <View>
             </View>     
                      <TouchableOpacity  onPress={()=> this.props.navigation.navigate("Login")}
                        style={[LoginStyles.SubmitButtonStyle,{width:"56%",height:moderateScale(44)}]}>
                            <Text style={LoginStyles.SignInTextStyle}>Login</Text>
                      </TouchableOpacity>
        </View>
      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    margin:20
  }
});