import React, { Component } from 'react';
import { SafeAreaView, Platform, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ImageBackground } from 'react-native';
import Colors from "../../Common/Colors";
import { LoginStyles } from '../../Common/Styles/LoginStyles';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import { EditText } from '../../Components/EditText';
import OTPInputView from '@twotalltotems/react-native-otp-input'

export default class ForgetPassword extends Component {

  componentDidMount() {
    console.disableYellowBox = true;

  }
  Submit() {
    this.props.navigation.navigate("ResetPassword")
  }

  render() {

    return (

      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />
      
          <Image resizeMode={"center"} style={[LoginStyles.logoStyles]}
            source={require('../Images/LogoImages/logo.png')} />
             <View >
          <Text style={{ color: "#FF3662", fontSize: moderateScale(28), marginStart: "10%", fontWeight: "bold" }}>
            Forget {'\n'}Password</Text>
         
  
            {EditText('Phone Number', require('../Images/LoginImages/lock.png'), null, null)}

              <View style={{top:verticalScale(25 ),position:'relative', alignSelf: "center", width: '90%', height: 1, backgroundColor: "lightgray" }} />
              <TouchableOpacity
                style={[LoginStyles.SubmitButtonStyle, { width: "46%", height: moderateScale(44) }]}>
                <Text style={LoginStyles.SignInTextStyle}>Send OTP</Text>
              </TouchableOpacity>
           

              <ImageBackground resizeMode='contain'source={require('../../MobileApp/Images/LoginImages/edittxt_bg.png')} 
         style={{
            alignItems: 'center',
            justifyContent:'center',
            height:moderateScale(68),
            width:'100%',
        }}>
<OTPInputView
    style={styles.otp}
    pinCount={4}
  //  code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
  // onCodeChanged = {code => { this.setState({code})}}
    //autoFocusOnLoad
    codeInputFieldStyle={styles.underlineStyleBase}
    codeInputHighlightStyle={styles.underlineStyleHighLighted}
    onCodeFilled = {(code => {this.Submit(),
        console.log(`Code is ${code}, you are good to go!`)
    })}
/>
</ImageBackground>
            {/* <TouchableOpacity onPress={() => this.Submit()}
              style={[LoginStyles.SubmitButtonStyle, { width: "56%", height: verticalScale(40) }]}>
              <Text style={LoginStyles.SignInTextStyle}>Submit</Text>
            </TouchableOpacity> */}
           </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    justifyContent:"space-around",
    paddingBottom:verticalScale(50)
  },
  otp:{
    width: '52%', height:50 ,
    marginBottom:scale(20)
  },
  underlineStyleBase: {
    width:scale(20),
    borderWidth: 0,
    borderBottomWidth:2,
  },

  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
  },
});