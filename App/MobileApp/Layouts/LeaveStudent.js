import React, { Component } from 'react';
import { YellowBox, Platform, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ImageBackground } from 'react-native';

import CalendarPicker from 'react-native-calendar-picker';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import { EditText } from '../../Components/EditText';
import { TextInput } from 'react-native-gesture-handler';
import { ComplaintStyles } from '../../Common/Styles/ComplaintStyles';
import { Dropdown } from "react-native-material-dropdown";

export default class LeaveStudent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          selectedStartDate: null,
        };
        this.onDateChange = this.onDateChange.bind(this);
      }
     
      
  onDateChange(date, type) {
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  }


  render() {

    let LeaveList = [{
        value: 'Sick',
      }, {
        value: 'Casual',
      }, {
        value: 'Others',
      }];

    const { selectedStartDate, selectedEndDate } = this.state;
    const minDate = new Date(); // Today
    const maxDate = new Date(2017, 6, 3);
    const startDate  =  selectedStartDate ? selectedStartDate.toString() : '';
    const endDate = selectedEndDate ? selectedEndDate.toString() : '';
    
    return (

      <View style={styles.container}>
       
        <View style={{margin:scale(10)}}>

        <Text style={ComplaintStyles.text}>Leave Type</Text>
<ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
    alignItems: 'center',
    alignSelf:'center',
    height:moderateScale(60),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    >
       <Dropdown
              textColor={"gray"}
              placeholder={"Select Leave Type"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20)
              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={LeaveList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>
          <Text style={ComplaintStyles.text}>Leave Priority</Text>
<ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
    alignItems: 'center',
    alignSelf:'center',
    height:moderateScale(60),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    >
       <Dropdown
              textColor={"gray"}
              placeholder={"Select Leave Priority"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20)
              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={LeaveList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>


          <Text style={ComplaintStyles.text}>Description</Text>
          <ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
          style={{
           alignSelf:'center',
             alignItems: 'center',
             height:moderateScale(60),
             flexDirection: 'row',
             width:scale(340),
             borderBottomColor:'#transparent',
             justifyContent:'center'}}>
          <TextInput keyboardType='email-address' 
              multiline = {true}
              style={{width:'85%',height:verticalScale(40),color:'gray'}} 
              selectionColor={"gray"}
              placeholderTextColor={'gray'}
              underlineColorAndroid='transparent'
              keyboardType='email-address'
            // onChangeText = {onChange}
            //    onSubmitEditing={() => { this.secondTextInput.focus(); }}
            />
          </ImageBackground>
          <CalendarPicker
          startFromMonday={true}
          allowRangeSelection={true}
          minDate={minDate}
          maxDate={maxDate}
          todayBackgroundColor="red"
          selectedDayColor="#7300e6"
          selectedDayTextColor="yellow"
          onDateChange={this.onDateChange}
        />
           {/* <TouchableOpacity  onPress={() =>this.props.navigation.navigate("Issues")}
            style={[ComplaintStyles.SubmitButtonStyle,{width:'35%'}]}>
        
              <Text style={ComplaintStyles.SignInTextStyle}>SUBMIT</Text>
            </TouchableOpacity> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
   // justifyContent:"center",
  },
  
});