import React, { Component } from 'react'
import { Text, View ,ImageBackground,StyleSheet,TouchableOpacity,Image} from 'react-native'
import { ComplaintStyles } from '../../Common/Styles/ComplaintStyles';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import { Dropdown } from "react-native-material-dropdown";
import Colors from '../../Common/Colors';
import { SafeAreaView } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';

export default class Notes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filePath: "",
      pdfname:"",pdfsize:""
    };
  }
  async chooseFile(){
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });

      this.setState({pdfname:res.name,pdfsize:res.size})
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

    render() {
        let ClassList = [{
            value: '1st',
          }, {
            value: '2nd',
          }, {
            value: '3rd',
          }];

let SubjectList = [{
    value: 'Maths',
  }, {
    value: 'Chemistry',
  }, {
    value: 'History',
  }];



        return (
            <View style={styles.container}>
{/* <SafeAreaView style={{flex:1,backgroundColor:Colors.color.themeYellow}}> */}
                <View style={{margin:scale(20)}}>

<Text style={ComplaintStyles.text}>Select Class</Text>
<ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
   alignSelf:'center',
    alignItems: 'center',
    height:moderateScale(68),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    >
       <Dropdown
              textColor={"gray"}
              placeholder={"Select"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20)
              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={ClassList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>
<Text style={ComplaintStyles.text}>Select Subject</Text>
<ImageBackground resizeMode='stretch' source={require('../Images/LoginImages/edittxt_bg.png')} 
 style={{
  alignSelf:'center',
    alignItems: 'center',
    height:moderateScale(68),
    flexDirection: 'row',
    width:scale(340),
    borderBottomColor:'#transparent',
justifyContent:'center'
}}
    >
       <Dropdown
              textColor={"gray"}
              placeholder={"Select"}
              placeholderTextColor={"gray"}
              inputContainerStyle={{
                borderBottomColor: "transparent",
                width:scale(280),
                justifyContent:"center",
                height:verticalScale(20)
              }}
              pickerStyle={{
                  alignSelf:'center',
                backgroundColor: Colors.color.card,
                width: scale(260),
              // marginLeft: wp("1.5%")
              }}
              itemPadding={8}
              dropdownOffset={{ top: 12, left: 0 }}
            //  rippleInsets={{ top: 0, bottom: 0 }}
              data={SubjectList}
              onChangeText={this.onChangeText2}
            />

          </ImageBackground>

<Text style={ComplaintStyles.text}>Upload PDF</Text>
<View style={styles.pdfBg}>
           <View style={styles.pdfborder}>
           <TouchableOpacity  onPress={()=>this.chooseFile()}>
           {this.state.pdfname == "" ?
<View style={{alignItems:'center',justifyContent:'center'}}>
<Image resizeMode="contain" source={require('../Images/ParentMenuImages/pdf.png')} style={{height:verticalScale(40),width:scale(45)}}/>
<Text style={{color:'dimgray'}}>Upload a PDF</Text></View>
: <Text numberOfLines={3} style={styles.pdfText}>
           {this.state.pdfname}{'\n'}size ({((this.state.pdfsize)/(1000))} kb)</Text>
           }
</TouchableOpacity>

         </View>
         </View>


    <View style={{height:'20%',justifyContent:'flex-end'}}> 

            <TouchableOpacity  onPress={() =>this.props.navigation.navigate("Issues")}
              style={ComplaintStyles.SubmitButtonStyle}>
          
                <Text style={ComplaintStyles.SignInTextStyle}>Done</Text>
              </TouchableOpacity>
              </View>
            </View>
            </View>
          
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    pdfBg:{ 
       width: '100%',
         height:verticalScale(150),
         borderRadius:scale(15),elevation:3,
         backgroundColor:Colors.color.card,
         padding: scale(15),alignItems:'center',
         marginVertical: verticalScale(10)
        },
         pdfborder:{
           borderStyle:'dashed', width: '100%',
         height:verticalScale(120),borderColor:'gray',
         borderWidth:2,alignItems:'center',justifyContent:'center',
         borderRadius:scale(15),
         },
         pdfText:{
           color:"dimgray",
           textAlign:'center',
           margin:10
         }
})