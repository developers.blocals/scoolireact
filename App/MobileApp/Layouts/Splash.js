import React, { Component } from 'react';
import { YellowBox, Platform, StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import Colors from "../../Common/Colors";
import { StackActions, NavigationActions } from 'react-navigation';
import { scale, verticalScale } from '../../Common/utils/scale';

export default class Splash extends Component {

  componentDidMount() {
    console.disableYellowBox = true;
    setTimeout(() => {
      this.navigateToAppropriateScreen()
    }, 3000);
  }
  navigateToAppropriateScreen() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {

    return (

      <View style={styles.container}>
       <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Image resizeMode={"center"} style={{ height: scale(225), width: scale(180), padding: 0 }}
            source={require('../Images/LogoImages/logo.png')} />

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  }
});