import React, { Component } from 'react';
import { YellowBox, Platform, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ImageBackground } from 'react-native';
import Colors from "../../Common/Colors";
import { StackActions, NavigationActions } from 'react-navigation';
import { scale, verticalScale, moderateScale } from '../../Common/utils/scale';
import { EditText } from '../../Components/EditText';
import { LoginStyles } from '../../Common/Styles/LoginStyles';
import { postApi } from '../../Common/utils/api';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }
  componentDidMount() {
    console.disableYellowBox = true;

  }
  test = () => {
    fetch('http://52.66.240.44:8080/scooli/mobile/authenticate', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      }),
    }).then((response) => response.json())
      .then((responseJson) => {
        alert(JSON.stringify(responseJson));
      })
      .catch((error) => {
        alert(error);
      });
  }
  navigateToHome = () => {
    this.test()
    //this.props.navigation.navigate('Drawer');
  }
  loginClicked = async () => {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',

    }
    if (this.state.username == '') {
      Toast.show('Please Enter Username');
    } else if (this.state.username == '') {
      Toast.show('Please Enter Password');
    } else {

      const object = {
        username: this.state.username,
        password: this.state.password
      }
      const res = await postApi(login, object, headers);

      // alert(JSON.stringify(res))
      if (res.status == 'true') {
        this.props.navigation.navigate('Drawer');
        alert(res.status);
      }
      else {
        Toast.show('Please Enter Correct Username and Password');
      }
    }
  }


  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.color.themeYellow}
          barStyle="light-content"
          animated={true}
        />
        <View style={{ flex: 1, justifyContent: "center", alignContent: "center" }}>
          <Image resizeMode={"center"} style={LoginStyles.logoStyles}
            source={require('../Images/LogoImages/logo.png')} />
          {EditText('Username', require('../Images/LoginImages/user.png'), null, null)}
          {EditText('Password', require('../Images/LoginImages/lock.png'), null, null)}
          <TouchableOpacity onPress={() => this.test()}
            style={LoginStyles.SubmitButtonStyle}>
            <Text style={LoginStyles.SignInTextStyle}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{ width: '100%' }} onPress={() => {
            this.props.navigation.navigate("ForgetPassword")
          }}>
            <Text style={{ textAlign: 'center', width: '90%', marginBottom: verticalScale(10), alignSelf: 'center', fontFamily: 'Avenir-Medium', marginEnd: scale(10), fontSize: scale(12), color: 'gray' }}>Forget reset password</Text>
            <View style={{ width: '33%', padding: 0, marginEnd: scale(6), height: 1, backgroundColor: 'pink', alignSelf: 'center' }}></View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  }
});