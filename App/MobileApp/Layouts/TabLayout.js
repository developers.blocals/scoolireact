import React, { Component } from 'react';
import { Animated, YellowBox, BackHandler, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, TextInput, StatusBar, Dimensions, ImageBackground, AsyncStorage } from 'react-native';

import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";

import AlertScreen from './AlertScreen';
import ParentHome from './ParentsHome';
import { scale, verticalScale } from '../../Common/utils/scale';
import StudentTt from './StudentTt';
import Colors from "../../Common/Colors";
import { Icon } from "react-native-elements";
import Notification from './Notification';
import { createAppContainer,createStackNavigator } from 'react-navigation';
import Message from './Chat';
import Homework from './Homework';
import Leave from './Leave';
import AttendanceGraph from './AttendanceGraph';
import Login from './Login';
import Issues from './Issues/Issues';
import CreateIssues from './Issues/CreateIssues';
import YourIssues from './Issues/YourIssues';
import AllIssues from './Issues/AllIssues';
import Notes from './Notes';
import Due from './Due';
import Chat from './Chat';
import Marks from './Marks';
import LeaveStudent from './LeaveStudent';
import TrackBus from './TrackBus';
import Complaints from './Complaints';

const baseNavigationOptions = {
    headerStyle: {backgroundColor: Colors.color.themeYellow},
    headerTintColor: '#FFF',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize:18
    },
  };
  
  const NotificationStack = createStackNavigator(
    {
        Notification: { screen: Notification ,
        navigationOptions:{
        ...baseNavigationOptions,
        headerTitle: 'Notification'
        }}
    }
  );

  const MessageStack = createStackNavigator(
    {
        Issues: { screen: Issues ,
        navigationOptions:{
        ...baseNavigationOptions,
        headerTitle: 'Issues'
        }},
        YourIssues: { screen: YourIssues ,
            navigationOptions:{
            ...baseNavigationOptions,
            headerTitle: 'Your Issues'
            }},
            CreateIssues: { screen: CreateIssues ,
                navigationOptions:{
                ...baseNavigationOptions,
                headerTitle: 'Create Issues'
                }},
                AllIssues: { screen: AllIssues ,
                    navigationOptions:{
                    ...baseNavigationOptions,
                    headerTitle: 'All Issues'
                    }},
    },

  );

  const ParentHomeStack = createStackNavigator(
    {
        ParentHome: { screen: ParentHome ,
            navigationOptions: {
                header: null,
            }        },
        Homework: {screen: Homework ,
            navigationOptions:{
                headerTitle: 'Homework'
                }},
                Leave: {screen: Leave ,
                    navigationOptions:{
                        headerTitle: 'Leave'
                        }},
                        AttendanceGraph: {screen: AttendanceGraph ,
                            navigationOptions:{
                                headerTitle: 'Attendance Graph'
                                }} ,
                                Notes: {screen: Notes ,
                                    navigationOptions:{
                                        headerTitle: 'Notes'
                                        }},
                                        Complaints: {screen: Complaints ,
                                            navigationOptions:{
                                                headerTitle: 'Complaints'
                                                }},
                                                Chat: {screen: Chat ,
                                                    navigationOptions:{
                                                        headerTitle: 'Chat with us'
                                                        }},
                                                        Marks: {screen: Marks ,
                                                            navigationOptions:{
                                                                headerTitle: 'Marks'
                                                                }}, 
                                                                TimeTable: {screen: StudentTt,
                                                                    navigationOptions:{
                                                                        headerTitle: 'Time Table'
                                                                        }}, 
                                                                        TrackBus: {screen: TrackBus,
                                                                            navigationOptions:{
                                                                                headerTitle: 'Track Bus'
                                                                                }}, 
                                                                        LeaveStudent: {screen: LeaveStudent,
                                                                            navigationOptions:{
                                                                                headerTitle: 'Leave Student'
                                                                                }},                  
                                        AlertScreen: {screen: AlertScreen ,
                                            navigationOptions:{
                                                headerTitle: 'Alerts'
                                                }},                 

                                Login: {screen: Login ,
                                    navigationOptions:{
                                        header: null,
                                    }}                        
    }
    , {initialRouteName:"ParentHome",
    defaultNavigationOptions: {
        ...baseNavigationOptions,
    }}

    );
export default createMaterialBottomTabNavigator({
    Message : {
        screen: MessageStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintcolor }) => (
                <Icon name="ios-chatboxes" type="ionicon" color={"#FFF"}/>
            ), }
    },

    Home: {
        screen: ParentHomeStack,
        navigationOptions: {
            tabBarIcon: ({ focused, tintcolor }) => (
                <Icon name="ios-home" type="ionicon" color={"#FFF"}/>
            )}
    },
    Notifications: {
        screen: NotificationStack,
        navigationOptions: {

            tabBarIcon: ({ focused, tintcolor }) => (
                <Icon name="ios-notifications" type="ionicon" color={"#FFF"}/>
            ),}
       }   

}, {
       initialRouteName: 'Home',
       shifting:true,
       showLabel:false,
        inactiveColor: '#f0edf6',
        activeColor:'#FFF',
       // activeColor: Colors.color.themeYellow,
        barStyle: { backgroundColor:Colors.color.themeRed },
        tabBarOptions:{showLabel:false, 
            labelStyle: {
            fontSize: 22,
            color:"#000"
          },}
    });




