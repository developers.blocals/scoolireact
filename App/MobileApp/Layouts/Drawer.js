import React, { Component } from 'react';
import { View, Image, SafeAreaView, ScrollView, Alert,Text,Share,StatusBar,Platform,TouchableOpacity} from 'react-native';
import { DrawerItems, createStackNavigator,createAppContainer,createDrawerNavigator,StackActions, NavigationActions } from 'react-navigation';
import { scale, verticalScale, moderateScale } from "../../Common/utils/scale";
import TabLayout from './TabLayout.js';
import Complaints from './Complaints.js';
import Colors from "../../Common/Colors";
import { Icon } from 'react-native-elements';

export default class Drawer extends Component {
    static navigationOptions = {
      gesturesEnabled: false
    };
    constructor(props) {
      super(props);
    }  
    render() {
        StatusBar.setBarStyle('light-content', true);
          return (
            <MyApp/>
          );
      }
    }

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    const Items = (props) => (
        <SafeAreaView style={{flex:1,backgroundColor:Colors.color.themeRed}}>
              <View style={{ height: verticalScale(260), alignItems: 'center', justifyContent: "space-evenly" }}>
                <Image source={require("../Images/Drawer/avatar.png")} resizeMode={"center"}
                 style={{height:verticalScale(120), width:verticalScale(120), borderRadius:verticalScale(60),marginTop:verticalScale(30) }} />
              <Text numberOfLines={1} style={{color:'#FFF',fontSize:scale(20),fontFamily:Platform.OS==='ios'?"Arial":'calibri',textAlign:'center'}}>
              Shweta Sethi</Text>
              <Text numberOfLines={1} style={{color:'#FFF',fontSize:scale(16),fontFamily:Platform.OS==='ios'?"Cochin":"calibri",textAlign:'center'}}>
              (Mother)</Text>
              <Text numberOfLines={1} 
              style={{margin:1,color:'#FFF',fontSize:scale(16),fontFamily:Platform.OS==='ios'?"Arial":"calibri",textAlign:'center'}}>+91-9787655667 </Text> 
              </View>
              {/* <DrawerItems 
              {...props}/> */}
              <TouchableOpacity>
              <View style={{flexDirection:"row",alignItems:"center",height:verticalScale(60),borderColor:"#FFF",
              borderTopWidth:0.6,borderBottomWidth:0.3,padding:scale(10)}}>
            <Icon name="ios-help-circle-outline" type="ionicon" color={"#FFF"}/>
          <Text style={{color:"#FFF",fontSize:scale(17),fontWeight:"500"}}>   Help Center</Text>
              </View>
              </TouchableOpacity>
              <TouchableOpacity>
              <View style={{flexDirection:"row",alignItems:"center",height:verticalScale(60),borderColor:"#FFF",
              borderTopWidth:0.3,borderBottomWidth:0.3,padding:10}}>
            <Icon name="ios-list-box" type="ionicon" color={"#FFF"}/>
          <Text style={{color:"#FFF",fontSize:scale(17),fontWeight:"500"}}>   Privacy Policy</Text>
              </View>
              </TouchableOpacity>
              <TouchableOpacity>
              <View style={{flexDirection:"row",alignItems:"center",height:verticalScale(60),borderColor:"#FFF",
              borderTopWidth:0.3,borderBottomWidth:0.3,padding:10}}>
            <Icon name="ios-clipboard" type="ionicon" color={"#FFF"}/>
          <Text style={{color:"#FFF",fontSize:scale(17),fontWeight:"500"}}>   Terms & Conditions</Text>
              </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>[props.navigation.closeDrawer(),
              Alert.alert(
               'Log out',
               'Do you want to logout?',
               [
                 {text: 'Cancel', onPress: () => {return null}},
                 {text: 'Confirm', onPress: () => {
                   props.navigation.dispatch(resetAction);
                 }},
               ],
               { cancelable: true }
             )]  }>
              <View style={{flexDirection:"row",alignItems:"center",height:verticalScale(50),borderColor:"#FFF",
              borderTopWidth:0.3,borderBottomWidth:0.6,padding:scale(10)}}>
            <Icon name="ios-log-out" type="ionicon" color={"#FFF"}/>
          <Text style={{color:"#FFF",fontSize:scale(17),fontWeight:"500"}}>   Logout</Text>
              </View>
              </TouchableOpacity>
          </SafeAreaView>
      )
    const AppDrawer = createDrawerNavigator({
        "  ": { screen:TabLayout },
        "Help Center":{screen:Complaints},
        "Terms & Conditions":{screen:Complaints},
         "Logout":{screen:Complaints}
      },
        {
            drawerPosition:'right',
            drawerWidth:scale(210),
            drawerBackgroundColor:Colors.color.themeRed,
          contentComponent: Items,
          contentOptions: {
        //    itemsContainerStyle: {
        //     marginTop:hp('-7.6%'),marginBottom:-4
        //   },
          LabelStyle:{
            fontSize:15,
            fontFamily:Platform.OS==='ios'?"Arial":"calibri"
          },
            activeTintColor: "#FFF"
          }
        })
      
    //   const HomeStack = createStackNavigator({
    //      AppDrawer: { screen: AppDrawer },
         
    //   })
    const MyApp = createAppContainer(AppDrawer);
