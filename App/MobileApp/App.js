
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Splash from './Layouts/Splash';
import Login from './Layouts/Login';
import ForgetPassword from './Layouts/ForgetPassword.js';
import ResetPassword from './Layouts/ResetPassword.js';
import PasswordChange from './Layouts/PasswordChange.js';
import Drawer from './Layouts/Drawer.js';

const App = createStackNavigator({
  Splash: { screen: Splash },
  Login: { screen: Login },
  ForgetPassword:{screen:ForgetPassword},
  ResetPassword:{screen:ResetPassword},
  PasswordChange:{screen:PasswordChange},
  Drawer:{screen:Drawer},
}
  , {initialRouteName:"Splash",
  defaultNavigationOptions: {
    header:null
  },
}
);

export default createAppContainer(App);