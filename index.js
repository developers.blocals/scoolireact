/** @format */
import { AppRegistry } from "react-native";
import {name as appName} from './app.json';
import App from './App/MobileApp/App';

AppRegistry.registerComponent(appName, () => App);